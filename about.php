<?php
$name = 'Вера Бочкова';
$age = 35;
$email = 'xxx@yandex.ru';
$town = 'Tула';
$aboutme = 'Начинающий программист'
?>

<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="utf-8">
  <title>1-1</title>
  <style>
            body {
                font-family: sans-serif;  
            }
            
            dl {
                display: table-row;
            }
            
            dt, dd {
                display: table-cell;
                padding: 5px 10px;
            }
        </style>
</head>

<body>
	<h1>Страница пользователя <?php echo $name;?></h1>
	 <dl>
            <dt>Имя</dt>
            <dd><?php echo $name;?></dd>
        </dl>
        <dl>
            <dt>Возраст</dt>
            <dd><?php echo $age;?></dd>
        </dl>
        <dl>
            <dt>Адрес электронной почты</dt>
            <dd><a href="mailto:<?php echo $email;?>"><?php echo $email;?></a></dd>
        </dl>
        <dl>
            <dt>Город</dt>
            <dd><?php echo $town;?></dd>
        </dl>
        <dl>
            <dt>О себе</dt>
            <dd><?php echo $aboutme;?></dd>
        </dl>
</body>

</html>